module.exports = {
  root: true,
  extends: '@react-native-community',
  plugins: ['import'],
  settings: {
    'import/resolver': {
      node: {
        paths: ['lib'],
        alias: {
          '@styles': './lib/styles',
          '@components': './lib/components',
          '@hooks': './lib/hooks',
          '@utils': './lib/utils',
        },
      },
    },
  },
};
