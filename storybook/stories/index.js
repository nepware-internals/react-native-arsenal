import React from 'react';
import {Text} from 'react-native';

import {storiesOf} from '@storybook/react-native';
import {action} from '@storybook/addon-actions';
import {linkTo} from '@storybook/addon-links';

import CenterView from './CenterView';
import Welcome from './Welcome';
import Slider from '@components/Slider';
import BottomDrawer from '@components/BottomDrawer';
import RadioInput from '@components/forms/RadioInput';
import Timer from '@components/Timer';
import Icon from '@components/Icons';

storiesOf('Welcome', module).add('to Storybook', () => (
  <Welcome showApp={linkTo('Button')} />
));

storiesOf('Slider', module)
  .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
  .add('Normal', () => <Slider />);

storiesOf('Bottom Drawer', module)
  .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
  .add('Normal', () => <BottomDrawer containerHeight={500} />);

storiesOf('Radio Input', module)
  .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
  .add('Small', () => <RadioInput label="Small" size="small" selected />)
  .add('Default', () => <RadioInput label="Default" selected />)
  .add('Large', () => (
    <RadioInput
      label="Large"
      size="large"
      selected
      labelStyle={{fontSize: 23, marginRight: 10}}
    />
  ));

storiesOf('Timer', module)
  .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
  .add('Default', () => <Timer seconds={3605} />)
  .add('Custom Renderer', () => (
    <Timer seconds={3600} renderer={(seconds) => seconds} />
  ));

storiesOf('Icons', module)
  .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
  .add('Icons', () => (
    <>
      <Icon provider="Entypo" name="github" size={20} color="black" />
      <Icon
        provider="MaterialCommunityIcons"
        name="crown"
        size={24}
        color="red"
      />
      <Icon
        provider="FontAwesome"
        name="soccer-ball-o"
        size={28}
        color="blue"
      />
      <Icon
        provider="Ionicons"
        name="ios-folder-open"
        size={32}
        color="orange"
      />
    </>
  ));
