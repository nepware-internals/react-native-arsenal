import React from 'react';
import {Text} from 'react-native';
import PropTypes from 'prop-types';

import TimeUtils from '@utils/time';

export default class Timer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {timer: props.seconds};
  }

  static propTypes = {
    /**
     * The number of seconds the timer starts with.
     */
    seconds: PropTypes.number.isRequired,

    /**
     * Used to format the timer value.
     * @param {number} seconds - The number of seconds remaining in the timer.
     */
    renderer: PropTypes.func,

    /**
     * Callback called when the timer completes.
     */
    onComplete: PropTypes.func,
  };

  componentDidMount() {
    this.interval = setInterval(
      () => this.setState((prevState) => ({timer: prevState.timer - 1})),
      1000,
    );
  }

  componentDidUpdate() {
    if (this.state.timer === 1) {
      clearInterval(this.interval);
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
    const {onComplete} = this.props;
    onComplete && onComplete();
  }

  render() {
    return (
      <Text style={this.props.style}>
        {this.props.renderer
          ? this.props.renderer(this.state.timer)
          : TimeUtils.formatTime(this.state.timer)}
      </Text>
    );
  }
}
