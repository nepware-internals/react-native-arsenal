import React, {useCallback, useState} from 'react';
import {View, Pressable, Platform, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';

import cs from '../../utils/cs';

const androidRippleOptions = {
  color: 'rgba(0,0,0,0.1)',
  borderless: true,
};

const Button = (props) => {
  const {
    children,
    style,
    pressableStyle,
    feedbackStyle,
    android_ripple: rippleProp,
    disableAndroidRipple,
    onPressIn,
    onPressOut,
    ...touchableProps
  } = props;

  const [isPressed, setPressed] = useState(false);

  const handlePressIn = useCallback(() => {
    setPressed(true);
    onPressIn && onPressIn();
  }, [onPressIn]);

  const handlePressOut = useCallback(() => {
    setPressed(false);
    onPressOut && onPressOut();
  }, [onPressOut]);

  return (
    <View style={style}>
      <Pressable
        style={cs(pressableStyle, [
          feedbackStyle ?? styles.touchFeedback,
          (Platform.OS !== 'android' || disableAndroidRipple) && isPressed,
        ])}
        onPressIn={handlePressIn}
        onPressOut={handlePressOut}
        android_ripple={
          disableAndroidRipple ? null : rippleProp ?? androidRippleOptions
        }
        {...touchableProps}>
        {children}
      </Pressable>
    </View>
  );
};

Button.propTypes = {
  /**
   * Style applied to View containing Pressable component. Don't use padding for proper feedback.
   */
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),

  /**
   * Style applied to Pressable component. Don't use margin for proper feedback.
   */
  pressableStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),

  /**
   * Configure feedback if not on android or default android ripple is not desired.
   * Needs disableAndroidRipple prop to be true to work on android.
   */
  feedbackStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),

  /**
   * Disables the default android ripple animation.
   * You may want to configure feedback with feedbackStyle prop.
   */
  disableAndroidRipple: PropTypes.bool,

  /**
   * Refer to Pressable Component in React Native Docs for other props
   */
};

const styles = StyleSheet.create({
  touchFeedback: {
    backgroundColor: 'rgba(0,0,0,0.1)',
    borderRadius: 12,
  },
});

export default Button;
