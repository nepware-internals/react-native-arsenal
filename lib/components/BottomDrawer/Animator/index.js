import React, {Component} from 'react';
import {
  PanResponder,
  Animated,
  Dimensions,
  StyleSheet,
  View,
} from 'react-native';

import cs from '@utils/cs';

import styles from './styles';

const SCREEN_HEIGHT = Dimensions.get('window').height;

export default class Animator extends Component {
  constructor(props) {
    super(props);

    this.position = new Animated.ValueXY(this.props.currentPosition);

    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: this.handlePanResponderMove,
      onPanResponderRelease: this.handlePanResponderRelease,
    });
  }

  render() {
    return (
      <Animated.View
        style={cs(
          {...this.position.getLayout(), left: 0},
          StyleSheet.flatten(
            cs(styles.animationContainer(this.props.containerHeight), [
              styles.roundedEdges,
              this.props.roundedEdges,
            ]),
          ),
          this.props.style,
        )}>
        <View
          style={cs(styles.drawerHandleContainer, [
            styles.roundedEdges,
            this.props.roundedEdges,
          ])}
          {...this.panResponder.panHandlers}>
          <View style={styles.drawerHandle} />
        </View>
        {this.props.children}
      </Animated.View>
    );
  }

  handlePanResponderMove = (e, gesture) => {
    if (this.swipeInBounds(gesture)) {
      this.position.setValue({y: this.props.currentPosition.y + gesture.dy});
    } else {
      this.position.setValue({
        y: this.props.upPosition.y - this.calculateEase(gesture),
      });
    }
  };

  handlePanResponderRelease = (e, gesture) => {
    if (
      gesture.dy > this.props.toggleThreshold &&
      this.props.currentPosition === this.props.upPosition
    ) {
      this.transitionTo(this.props.downPosition, this.props.onCollapsed);
    } else if (
      gesture.dy < -this.props.toggleThreshold &&
      this.props.currentPosition === this.props.downPosition
    ) {
      this.transitionTo(this.props.upPosition, this.props.onExpanded);
    } else if (
      gesture.dy > this.props.toggleThreshold &&
      this.props.currentPosition === this.props.downPosition
    ) {
      this.props.onClose();
    } else {
      this.resetPosition();
    }
  };

  // returns true if the swipe is within the height of the drawer.
  swipeInBounds(gesture) {
    return this.props.currentPosition.y + gesture.dy > this.props.upPosition.y;
  }

  calculateEase(gesture) {
    return Math.min(Math.sqrt(gesture.dy * -1), Math.sqrt(SCREEN_HEIGHT));
  }

  transitionTo(position, callback) {
    Animated.spring(this.position, {
      toValue: position,
    }).start(() => this.props.onExpanded());

    this.props.setCurrentPosition(position);
    callback();
  }

  resetPosition() {
    Animated.spring(this.position, {
      toValue: this.props.currentPosition,
    }).start();
  }
}
