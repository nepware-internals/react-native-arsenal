import {Dimensions} from 'react-native';

const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

export default {
  animationContainer: (height) => ({
    width: SCREEN_WIDTH,
    position: 'absolute',
    height: height + Math.sqrt(SCREEN_HEIGHT),
    backgroundColor: '#fff',
  }),
  roundedEdges: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  drawerHandleContainer: {
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
    height: '5%',
    backgroundColor: '#fff',
  },
  drawerHandle: {
    backgroundColor: '#ccc',
    width: '20%',
    height: '25%',
    alignSelf: 'center',
    borderRadius: 10,
  },
};
