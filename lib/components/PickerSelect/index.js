import React, {useState, useCallback} from 'react';
import {View} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import PropTypes from 'prop-types';

const PickerSelect = (props) => {
  const {
    style,
    itemStyle,
    wrapperStyle,
    disabled,
    valueExtractor,
    itemLabelExtractor,
    initialSelectedValue,
    onValueChange,
    data,
    color = 'black',
    fontFamily,
  } = props;

  const [itemValue, setItemValue] = useState(initialSelectedValue);

  const onChange = useCallback(
    (value) => {
      setItemValue(value);
      onValueChange && onValueChange(value);
    },
    [onValueChange],
  );

  return (
    <View style={wrapperStyle}>
      <Picker
        style={style}
        itemStyle={itemStyle}
        mode="dropdown"
        selectedValue={itemValue}
        onValueChange={onChange}
        enabled={!disabled}
        dropdownIconColor={color}>
        {data?.map((item) => {
          const value = valueExtractor ? valueExtractor(item) : item.id;

          return (
            <Picker.Item
              fontFamily={fontFamily}
              style={style}
              label={item.name || itemLabelExtractor(item)}
              value={value}
              key={value}
              color={itemValue === value ? color : 'black'}
            />
          );
        })}
      </Picker>
    </View>
  );
};

PickerSelect.propTypes = {
  /**
   * Style applied to picker component
   */
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),

    /**
   * Style applied to picker component
   */
     itemStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),

  /**
   * Style applied to wrapping View component
   */
  wrapperStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),

  /**
   * Whether the Picker is disabled
   */
  disabled: PropTypes.bool,

  /**
   * Function to get value from data item. Takes id as default.
   */
  valueExtractor: PropTypes.func,

  /**
   * Function to get label from data item. Takes name property as default.
   */
  itemLabelExtractor: PropTypes.func,

  /**
   * Initial value that is selected. Must be a value from a data item.
   */
  initialSelectedValue: PropTypes.any,

  /**
   * Callback called when the selected item is changed
   */
  onValueChange: PropTypes.func,

  /**
   * Array of picker items.
   */
  data: PropTypes.array.isRequired,

  /**
   * Color of selected label.
   */
  color: PropTypes.string,

  /**
   * Font family of labels.
   */
  fontFamily: PropTypes.string,
};

export default PickerSelect;
