import React from 'react';
import {View} from 'react-native';
import {useTabContext} from './TabContext';

const TabContent = React.forwardRef((props, ref) => {
  const {active} = useTabContext(props);

  if (!active) {
    return null;
  }

  return <View ref={ref} {...props} />;
});

export default TabContent;
