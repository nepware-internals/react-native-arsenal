import React, {useCallback} from 'react';
import {Text} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

import {useTabContext} from './TabContext.js';

import cs from '../../utils/cs';

const TabHeader = props => {
  const {
    title,
    index,
    style,
    activeStyle,
    titleStyle,
    activeTitleStyle,
    active,
    selectTab,
    label,
    ...childProps
  } = useTabContext(props);

  const handlePress = useCallback(() => {
    selectTab && selectTab(label);
  }, [selectTab, label]);

  if (!title) {
    return null;
  }

  return (
    <TouchableOpacity
      style={cs(style, [activeStyle, active])}
      onPress={handlePress}
      {...childProps}>
      <Text style={cs(titleStyle, [activeTitleStyle, active])}>{title}</Text>
    </TouchableOpacity>
  );
};

export default TabHeader;
