import React, {useMemo, useCallback, useRef, useState} from 'react';
import {FlatList} from 'react-native';
import PropTypes from 'prop-types';

import TabContext from './TabContext';
import TabHeader from './TabHeader';
import TabContent from './TabContent';

const getChildren = (children) => React.Children.toArray(children);
const getDefaultActiveTab = (children) => children?.[0]?.props?.label;

export const Tab = () => null;

const Tabs = (props) => {
  const {children} = props;
  const _children = getChildren(children);

  const {
    onChange,
    defaultActiveTab = getDefaultActiveTab(_children),
    headerStyle,
    tabItemStyle,
    tabLabelStyle,
    activeTabLabelStyle,
    activeTabItemStyle,
    contentContainerStyle,
  } = props;

  const tabsRef = useRef();
  tabsRef.current = new Array(_children.length);

  const [activeTab, setActiveTab] = useState(defaultActiveTab);

  const tabContext = useMemo(
    () => ({
      selectTab: (label, index) => {
        onChange &&
          onChange({
            activeTab: label,
            previousTab: tabContext.activeTab,
          });
        setActiveTab(label);
      },
      activeTab,
    }),
    [activeTab, onChange],
  );

  const renderTabHeader = useCallback(
    ({item, index}) => (
      <TabHeader
        index={index}
        style={tabItemStyle}
        activeStyle={activeTabItemStyle}
        titleStyle={tabLabelStyle}
        activeTitleStyle={activeTabLabelStyle}
        {...item.props}
      />
    ),
    [tabItemStyle, activeTabItemStyle, tabLabelStyle, activeTabLabelStyle],
  );

  const renderTabContent = useCallback(
    ({item, index}) => (
      <TabContent ref={(el) => (tabsRef.current[index] = el)} {...item.props} />
    ),
    [],
  );

  return (
    <TabContext.Provider value={tabContext}>
      <FlatList
        horizontal
        style={headerStyle}
        data={_children}
        keyExtractor={(item) => item.props.label}
        renderItem={renderTabHeader}
      />
      <FlatList
        style={contentContainerStyle}
        data={_children}
        keyExtractor={(item) => item.props.label}
        renderItem={renderTabContent}
      />
    </TabContext.Provider>
  );
};

Tabs.propTypes = {
  /**
   * Callback called when another tab is selected
   */
  onChange: PropTypes.func,

  /**
   * Label of tab that is active to start with. Default is the first tab.
   */
  defaultActiveTab: PropTypes.string,

  /**
   * Style applied to Tab Header Container
   */
  headerStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),

  /**
   * Style applied to each Tab Header Item
   */
  tabItemStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),

  /**
   * Style applied to each Tab Header Label
   */
  tabLabelStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),

  /**
   * Style applied to active Tab Header Label. Also uses styles from tabLabelStyle unless overridden.
   */
  activeTabLabelStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),

  /**
   * Style applied to active Tab Header Item. Also uses styles from tabItemStyle unless overridden.
   */
  activeTabItemStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),

  /**
   * Style applied to container of the Tabs content. Do not use alignItems, justifyContent, and other layout styles
   */
  contentContainerStyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
  ]),
};

export default Tabs;
