'use strict';

import React, {PureComponent} from 'react';

import {
  Animated,
  Image,
  PanResponder,
  View,
  Easing,
  ViewPropTypes,
} from 'react-native';

import PropTypes from 'prop-types';

import defaultStyles from './styles';

function Rect(x, y, width, height) {
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
}

Rect.prototype.containsPoint = function (x, y) {
  return (
    x >= this.x &&
    y >= this.y &&
    x <= this.x + this.width &&
    y <= this.y + this.height
  );
};

const DEFAULT_ANIMATION_CONFIGS = {
  spring: {
    friction: 7,
    tension: 100,
  },
  timing: {
    duration: 150,
    easing: Easing.inOut(Easing.ease),
    delay: 0,
  },
  // decay : { // This has a serious bug
  //   velocity     : 1,
  //   deceleration : 0.997
  // }
};

export default class Slider extends PureComponent {
  static propTypes = {
    /**
     * Initial value of the slider. The value should be between minimumValue
     * and maximumValue, which default to 0 and 1 respectively.
     * Default value is 0.
     *
     * *This is not a controlled component*, e.g. if you don't update
     * the value, the component won't be reset to its inital value.
     */
    value: PropTypes.number,

    /**
     * Value of the buffer track. The value should be between minimumValue
     * and maximumValue, which default to 0 and 1 respectively.
     * Default value is 0.
     */
    bufferValue: PropTypes.number,

    /**
     * If true the user won't be able to move the slider.
     * Default value is false.
     */
    disabled: PropTypes.bool,

    /**
     * Initial minimum value of the slider. Default value is 0.
     */
    minimumValue: PropTypes.number,

    /**
     * If the buffer track is shown. Default value is false.
     */
    showBufferTrack: PropTypes.bool,

    /**
     * Initial maximum value of the slider. Default value is 1.
     */
    maximumValue: PropTypes.number,

    /**
     * Step value of the slider. The value should be between 0 and
     * (maximumValue - minimumValue). Default value is 0.
     */
    step: PropTypes.number,

    /**
     * The color used for the track to the left of the button. Overrides the
     * default blue gradient image.
     */
    minimumTrackTintColor: PropTypes.string,

    /**
     * The color used for the buffer track.
     */
    bufferTrackTintColor: PropTypes.string,

    /**
     * The color used for the track to the right of the button. Overrides the
     * default blue gradient image.
     */
    maximumTrackTintColor: PropTypes.string,

    /**
     * The color used for the thumb.
     */
    thumbTintColor: PropTypes.string,

    /**
     * The size of the touch area that allows moving the thumb.
     * The touch area has the same center has the visible thumb.
     * This allows to have a visually small thumb while still allowing the user
     * to move it easily.
     * The default is {width: 40, height: 40}.
     */
    thumbTouchSize: PropTypes.shape({
      width: PropTypes.number,
      height: PropTypes.number,
    }),

    /**
     * Callback continuously called while the user is dragging the slider.
     */
    onValueChange: PropTypes.func,

    /**
     * Callback called when the user starts changing the value (e.g. when
     * the slider is pressed).
     */
    onSlidingStart: PropTypes.func,

    /**
     * Callback called when the user finishes changing the value (e.g. when
     * the slider is released).
     */
    onSlidingComplete: PropTypes.func,

    /**
     * The style applied to the slider container.
     */
    style: ViewPropTypes.style,

    /**
     * The style applied to the track.
     */
    trackStyle: ViewPropTypes.style,

    /**
     * The style applied to the thumb.
     */
    thumbStyle: ViewPropTypes.style,

    /**
     * Sets an image for the thumb.
     */
    thumbImage: Image.propTypes.source,

    /**
     * Set this to true to visually see the thumb touch rect in green.
     */
    debugTouchArea: PropTypes.bool,

    /**
     * Set to true to animate values with default 'timing' animation type
     */
    animateTransitions: PropTypes.bool,

    /**
     * Custom Animation type. 'spring' or 'timing'.
     */
    animationType: PropTypes.oneOf(['spring', 'timing']),

    /**
     * Used to configure the animation parameters.  These are the same parameters in the Animated library.
     */
    animationConfig: PropTypes.object,
  };

  static defaultProps = {
    value: 0,
    bufferValue: 0,
    minimumValue: 0,
    maximumValue: 1,
    step: 0,
    showBufferTrack: false,
    minimumTrackTintColor: '#3f3f3f',
    bufferTrackTintColor: '#969696',
    maximumTrackTintColor: '#b3b3b3',
    thumbTintColor: '#343434',
    thumbTouchSize: {width: 40, height: 40},
    debugTouchArea: false,
    animationType: 'timing',
  };

  state = {
    containerSize: {width: 0, height: 0},
    trackSize: {width: 0, height: 0},
    thumbSize: {width: 0, height: 0},
    allMeasured: false,
    value: new Animated.Value(this.props.value),
    bufferValue: new Animated.Value(this.props.bufferValue),
  };

  UNSAFE_componentWillMount() {
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: this.handleStartShouldSetPanResponder,
      onMoveShouldSetPanResponder: this.handleMoveShouldSetPanResponder,
      onPanResponderGrant: this.handlePanResponderGrant,
      onPanResponderMove: this.handlePanResponderMove,
      onPanResponderRelease: this.handlePanResponderEnd,
      onPanResponderTerminationRequest: this.handlePanResponderRequestEnd,
      onPanResponderTerminate: this.handlePanResponderEnd,
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    let newValue = nextProps.value;
    let newBufferValue = nextProps.bufferValue;

    if (this.props.value !== newValue) {
      if (this.props.animateTransitions) {
        this.setCurrentValueAnimated(newValue);
      } else {
        this.setCurrentValue(newValue);
      }
    }

    if (
      this.props.showBufferTrack &&
      this.props.bufferValue !== newBufferValue
    ) {
      this.setCurrentBufferValueAnimated(newBufferValue);
    }
  }

  render() {
    let {
      minimumValue,
      maximumValue,
      minimumTrackTintColor,
      bufferTrackTintColor,
      maximumTrackTintColor,
      thumbTintColor,
      thumbImage,
      showBufferTrack,
      styles,
      style,
      trackStyle,
      thumbStyle,
      debugTouchArea,
      ...other
    } = this.props;
    let {value, bufferValue, containerSize, thumbSize, allMeasured} =
      this.state;
    let mainStyles = styles || defaultStyles;
    let thumbLeft = value.interpolate({
      inputRange: [minimumValue, maximumValue],
      outputRange: [0, containerSize.width - thumbSize.width],
      //extrapolate: 'clamp',
    });
    let valueVisibleStyle = {};
    if (!allMeasured) {
      valueVisibleStyle.opacity = 0;
    }

    let minimumTrackStyle = {
      position: 'absolute',
      width: Animated.add(thumbLeft, thumbSize.width / 2),
      backgroundColor: minimumTrackTintColor,
      ...valueVisibleStyle,
    };

    let bufferWidth = bufferValue.interpolate({
      inputRange: [minimumValue, maximumValue],
      outputRange: [0, containerSize.width],
    });

    let bufferTrackStyle = {
      position: 'absolute',
      left: thumbLeft,
      width: Animated.subtract(bufferWidth, thumbLeft),
      backgroundColor: bufferTrackTintColor,
      ...valueVisibleStyle,
    };

    let touchOverflowStyle = this.getTouchOverflowStyle();

    return (
      <View
        {...other}
        style={[mainStyles.container, style]}
        onLayout={this.measureContainer}
        onStartShouldSetResponder={(evt) => true}
        onResponderRelease={this.handleTrackResponderRelease}>
        <View
          style={[
            {backgroundColor: maximumTrackTintColor},
            mainStyles.track,
            trackStyle,
          ]}
          renderToHardwareTextureAndroid={true}
          onLayout={this.measureTrack}
        />
        <Animated.View
          renderToHardwareTextureAndroid={true}
          style={[mainStyles.track, trackStyle, minimumTrackStyle]}
        />
        {showBufferTrack && (
          <Animated.View
            renderToHardwareTextureAndroid={true}
            style={[mainStyles.track, trackStyle, bufferTrackStyle]}
          />
        )}
        <Animated.View
          onLayout={this.measureThumb}
          renderToHardwareTextureAndroid={true}
          style={[
            {backgroundColor: thumbTintColor},
            mainStyles.thumb,
            thumbStyle,
            {
              transform: [{translateX: thumbLeft}, {translateY: 0}],
              ...valueVisibleStyle,
            },
          ]}>
          {this.renderThumbImage()}
        </Animated.View>
        <View
          renderToHardwareTextureAndroid={true}
          style={[defaultStyles.touchArea, touchOverflowStyle]}
          {...this.panResponder.panHandlers}>
          {debugTouchArea === true && this.renderDebugThumbTouchRect(thumbLeft)}
        </View>
      </View>
    );
  }

  getPropsForComponentUpdate(props) {
    let {
      value,
      onValueChange,
      onSlidingStart,
      onSlidingComplete,
      style,
      trackStyle,
      thumbStyle,
      ...otherProps
    } = props;

    return otherProps;
  }

  handleStartShouldSetPanResponder = (
    e: Object /*gestureState: Object*/,
  ): boolean => {
    // Should we become active when the user presses down on the thumb?
    return this.thumbHitTest(e);
  };

  handleMoveShouldSetPanResponder(/*e: Object, gestureState: Object*/): boolean {
    // Should we become active when the user moves a touch over the thumb?
    return false;
  }

  handlePanResponderGrant = (/*e: Object, gestureState: Object*/) => {
    this._previousLeft = this.getThumbLeft(this.getCurrentValue());
    this.fireChangeEvent('onSlidingStart');
  };

  handlePanResponderMove = (e: Object, gestureState: Object) => {
    if (this.props.disabled) {
      return;
    }

    this.setCurrentValue(this.getValue(gestureState));
    this.fireChangeEvent('onValueChange');
  };

  handlePanResponderRequestEnd(e: Object, gestureState: Object) {
    // Should we allow another component to take over this pan?
    return false;
  }

  handlePanResponderEnd = (e: Object, gestureState: Object) => {
    if (this.props.disabled) {
      return;
    }

    this.setCurrentValue(this.getValue(gestureState));
    this.fireChangeEvent('onSlidingComplete');
  };

  handleTrackResponderRelease = (e: Object) => {
    if (this.props.disabled) {
      return;
    }
    const value = e.nativeEvent.locationX / this.state.trackSize.width;
    this.setCurrentValue(value > 1 ? 1 : value);
    this.fireChangeEvent('onValueChange');
  };

  measureContainer = (x: Object) => {
    this.handleMeasure('containerSize', x);
  };

  measureTrack = (x: Object) => {
    this.handleMeasure('trackSize', x);
  };

  measureThumb = (x: Object) => {
    this.handleMeasure('thumbSize', x);
  };

  handleMeasure = (name: string, x: Object) => {
    let {width, height} = x.nativeEvent.layout;
    let size = {width: width, height: height};

    let storeName = `_${name}`;
    let currentSize = this[storeName];
    if (
      currentSize &&
      width === currentSize.width &&
      height === currentSize.height
    ) {
      return;
    }
    this[storeName] = size;

    if (this._containerSize && this._trackSize && this._thumbSize) {
      this.setState({
        containerSize: this._containerSize,
        trackSize: this._trackSize,
        thumbSize: this._thumbSize,
        allMeasured: true,
      });
    }
  };

  getRatio = (value: number) => {
    return (
      (value - this.props.minimumValue) /
      (this.props.maximumValue - this.props.minimumValue)
    );
  };

  getThumbLeft = (value: number) => {
    let ratio = this.getRatio(value);
    return (
      ratio * (this.state.containerSize.width - this.state.thumbSize.width)
    );
  };

  getValue = (gestureState: Object) => {
    let length = this.state.containerSize.width - this.state.thumbSize.width;
    let thumbLeft = this._previousLeft + gestureState.dx;

    let ratio = thumbLeft / length;

    if (this.props.step) {
      return Math.max(
        this.props.minimumValue,
        Math.min(
          this.props.maximumValue,
          this.props.minimumValue +
            Math.round(
              (ratio * (this.props.maximumValue - this.props.minimumValue)) /
                this.props.step,
            ) *
              this.props.step,
        ),
      );
    } else {
      return Math.max(
        this.props.minimumValue,
        Math.min(
          this.props.maximumValue,
          ratio * (this.props.maximumValue - this.props.minimumValue) +
            this.props.minimumValue,
        ),
      );
    }
  };

  getCurrentValue = () => {
    return this.state.value.__getValue();
  };

  setCurrentValue = (value: number) => {
    this.state.value.setValue(value);
  };

  setCurrentValueAnimated = (value: number) => {
    let animationType = this.props.animationType;
    let animationConfig = Object.assign(
      {},
      DEFAULT_ANIMATION_CONFIGS[animationType],
      this.props.animationConfig,
      {toValue: value},
    );

    Animated[animationType](this.state.value, animationConfig).start();
  };

  setCurrentBufferValueAnimated = (value: number) => {
    let animationType = this.props.animationType;
    let animationConfig = Object.assign(
      {useNativeDriver: false},
      DEFAULT_ANIMATION_CONFIGS[animationType],
      this.props.animationConfig,
      {toValue: value},
    );

    Animated[animationType](this.state.bufferValue, animationConfig).start();
  };

  fireChangeEvent = (event) => {
    if (this.props[event]) {
      this.props[event](this.getCurrentValue());
    }
  };

  getTouchOverflowSize = () => {
    let state = this.state;
    let props = this.props;

    let size = {};
    if (state.allMeasured === true) {
      size.width = Math.max(
        0,
        props.thumbTouchSize.width - state.thumbSize.width,
      );
      size.height = Math.max(
        0,
        props.thumbTouchSize.height - state.containerSize.height,
      );
    }

    return size;
  };

  getTouchOverflowStyle = () => {
    let {width, height} = this.getTouchOverflowSize();

    let touchOverflowStyle = {};
    if (width !== undefined && height !== undefined) {
      let verticalMargin = -height / 2;
      touchOverflowStyle.marginTop = verticalMargin;
      touchOverflowStyle.marginBottom = verticalMargin;

      let horizontalMargin = -width / 2;
      touchOverflowStyle.marginLeft = horizontalMargin;
      touchOverflowStyle.marginRight = horizontalMargin;
    }

    if (this.props.debugTouchArea === true) {
      touchOverflowStyle.backgroundColor = 'orange';
      touchOverflowStyle.opacity = 0.5;
    }

    return touchOverflowStyle;
  };

  thumbHitTest = (e: Object) => {
    let nativeEvent = e.nativeEvent;
    let thumbTouchRect = this.getThumbTouchRect();
    return thumbTouchRect.containsPoint(
      nativeEvent.locationX,
      nativeEvent.locationY,
    );
  };

  getThumbTouchRect = () => {
    let state = this.state;
    let props = this.props;
    let touchOverflowSize = this.getTouchOverflowSize();

    return new Rect(
      touchOverflowSize.width / 2 +
        this.getThumbLeft(this.getCurrentValue()) +
        (state.thumbSize.width - props.thumbTouchSize.width) / 2,
      touchOverflowSize.height / 2 +
        (state.containerSize.height - props.thumbTouchSize.height) / 2,
      props.thumbTouchSize.width,
      props.thumbTouchSize.height,
    );
  };

  renderDebugThumbTouchRect = (thumbLeft) => {
    let thumbTouchRect = this.getThumbTouchRect();
    let positionStyle = {
      left: thumbLeft,
      top: thumbTouchRect.y,
      width: thumbTouchRect.width,
      height: thumbTouchRect.height,
    };

    return (
      <Animated.View
        style={[defaultStyles.debugThumbTouchArea, positionStyle]}
        pointerEvents="none"
      />
    );
  };

  renderThumbImage = () => {
    let {thumbImage} = this.props;

    if (!thumbImage) {
      return;
    }

    return <Image source={thumbImage} />;
  };
}
