import React from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';

import RadioButton from './RadioButton';

const RadioInput = (props) => {
  const {label, labelStyle, onPress, contentContainerStyle, radioStyle, disabled} = props;

  return (
    <TouchableOpacity
      style={[styles.container, contentContainerStyle]}
      onPress={onPress}
      disabled={disabled}>
      <Text style={[styles.label, labelStyle]}>{label}</Text>
      <RadioButton style={radioStyle} {...props} disabled={disabled} />
    </TouchableOpacity>
  );
};

RadioInput.propTypes = {
  /**
   * The label for the radio option.
   */
  label: PropTypes.string.isRequired,

  /**
   * Style object for the content container.
   */
  contentContainerStyle: PropTypes.object,

  /**
   * Style object for the label text.
   */
  labelStyle: PropTypes.object,

  /**
   * Callback called when the user presses the input.
   */
  onPress: PropTypes.func,

  /**
   * Style applied to radio button.
   */
  radioStyle: PropTypes.object,

  /**
   * Whether input is disabled.
   */
  disabled: PropTypes.bool,
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 8,
  },
  label: {
    marginRight: 10,
    fontSize: 17,
  },
});

export default RadioInput;
