import React, {useMemo} from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';

import cs from '../../../../utils/cs';

const RadioButton = (props) => {
  const {
    selected = false,
    onPress,
    unSelectedColor = 'grey',
    color = 'black',
    size,
    style,
    disabled,
  } = props;

  const dimensions = useMemo(
    () =>
      size === 'small'
        ? {outerDiameter: 17, innerDiameter: 9, borderWidth: 2}
        : size === 'large'
        ? {outerDiameter: 34, innerDiameter: 19, borderWidth: 3}
        : {outerDiameter: 27, innerDiameter: 14, borderWidth: 2},
    [size],
  );

  return (
    <TouchableOpacity onPress={onPress} disabled={disabled}>
      <View
        style={cs(styles.outer, {
          borderColor: selected ? color : unSelectedColor,
          height: dimensions.outerDiameter,
          width: dimensions.outerDiameter,
          borderWidth: dimensions.borderWidth,
        }, style)}>
        {selected && (
          <View
            style={[
              styles.inner,
              {
                backgroundColor: color,
                height: dimensions.innerDiameter,
                width: dimensions.innerDiameter,
              },
            ]}
          />
        )}
      </View>
    </TouchableOpacity>
  );
};

RadioButton.propTypes = {
  /**
   * Represents whether the Radio button is filled(true) or not.
   * Defaults to false.
   */
  selected: PropTypes.bool,

  /**
   * Callback called when the user presses the radio.
   */
  onPress: PropTypes.func,

  /**
   * The color of the radio button.
   * Defaults to black.
   */
  color: PropTypes.string,

  /**
   * The size of the radio button: 'small' or 'large'.
   * No size prop renders the default size.
   */
  size: PropTypes.string,

  /**
   * Style applied to radio button.
   */
  style: PropTypes.object,

  /**
   * Whether input is disabled.
   */
  disabled: PropTypes.bool,
};

const styles = StyleSheet.create({
  outer: {
    flex: 0,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 17,
    padding: 3,
  },
  inner: {
    borderRadius: 17,
  },
});

export default RadioButton;
