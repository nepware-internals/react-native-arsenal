import {useEffect, useState} from 'react';
import {Dimensions} from 'react-native';

const useScreenDimension = () => {
  const [screenData, setScreenData] = useState(Dimensions.get('screen'));

  useEffect(() => {
    const onChange = (result) => {
      setScreenData(result.screen);
    };
    Dimensions.addEventListener('change', onChange);
  });
  return {
    ...screenData,
    isLandScape: screenData.width > screenData.height,
  };
};

export default useScreenDimension;
