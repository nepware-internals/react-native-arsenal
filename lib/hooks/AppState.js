import {useEffect, useState} from 'react';
import {AppState} from 'react-native';

const useAppState = () => {
  const [appState, setAppState] = useState(true);
  const [isForeground, setIsForeground] = useState(true);

  useEffect(() => {
    function handleAppStateChange(nextAppState) {
      if (appState.match(/inactive|background/) && nextAppState === 'active') {
        setIsForeground(true);
      } else if (
        nextAppState.match(/inactive|background/) &&
        appState === 'active'
      ) {
        setIsForeground(false);
      }
      setAppState(nextAppState);
    }
    AppState.addEventListener('change', handleAppStateChange);
    return () => {
      AppState.removeEventListener('change', this.handleAppStateChange);
    };
  });
  return {
    appState,
    isForeground,
  };
};

export default useAppState;
