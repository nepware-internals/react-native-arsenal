const lang = {
  en: {},
  ne: {},
};

const initialState = {
  selectedLanguage: 'en',
  availableLanguages: [
    {code: 'en', title: 'English'},
    {code: 'ne', title: 'Nepalese'},
  ],
  languages: lang,
};

const i18nReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_LANGUAGE':
      const newState = {selectedLanguage: action.payload, ...state};
      return newState;
    default:
      return state;
  }
};

export default i18nReducer;
