class TimeUtils {
  formatTime(seconds) {
    const ss = Math.floor(seconds) % 60;
    const mm = Math.floor(seconds / 60) % 60;
    const hh = Math.floor(seconds / 3600);

    if (hh > 0) {
      return (
        hh + ':' + this.formatTwoDigits(mm) + ':' + this.formatTwoDigits(ss)
      );
    } else {
      return mm + ':' + this.formatTwoDigits(ss);
    }
  }

  formatTwoDigits(n) {
    return n < 10 ? '0' + n : n;
  }

  get12HourTimeString(date) {
    const amOrPm = date.getHours() < 12 ? 'AM' : 'PM';
    const hour = (date.getHours() % 12).toString().padStart(2, '0');

    const time = new Date(date).toTimeString().substring(2, 5);
    return `${hour === '00' ? 12 : hour}${time} ${amOrPm}`;
  }

  getDuration(duration, unit = 'seconds') {
    let [days, timeString] = duration.split(' ');
    if (!timeString) {
      return '';
    }
    const [hours, minutes, seconds] = timeString?.split(':');
    if (unit === 'months' && timeString) {
      return +(+days / 30);
    }
    if (unit === 'days' && timeString) {
      return days;
    }
    return +days * 24 * 60 * 60 + hours * 60 * 60 + +minutes * 60 + +seconds;
  }

  getFutureTimeFromDuration(duration) {
    const totalSeconds = this.getDuration(duration);
    return new Date(+new Date() + totalSeconds * 1000);
  }

  timeSince(time, isDuration) {
    if (isDuration) {
      time = this.getFutureTimeFromDuration(time);
    }

    switch (typeof time) {
      case 'number':
        break;
      case 'string':
        time = +new Date(time);
        break;
      case 'object':
        if (time.constructor === Date) {
          time = time.getTime();
        }
        break;
      default:
        time = +new Date();
    }
    const time_formats = [
      [60, 'seconds', 1], // 60
      [120, '1 minute ago', '1 minute from now'], // 60*2
      [3600, 'minutes', 60], // 60*60, 60
      [7200, '1 hour ago', '1 hour from now'], // 60*60*2
      [86400, 'hours', 3600], // 60*60*24, 60*60
      [172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
      [604800, 'days', 86400], // 60*60*24*7, 60*60*24
      [1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
      [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
      [4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
      [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
      [58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
      [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
      [5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
      [58060800000, 'centuries', 2903040000], // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
    ];
    let seconds = (+new Date() - time) / 1000,
      token = 'ago',
      list_choice = 1;

    if (seconds === 0) {
      return 'Just now';
    }
    if (seconds < 0) {
      seconds = Math.abs(seconds);
      token = 'from now';
      list_choice = 2;
    }
    let i = 0,
      format;
    while ((format = time_formats[i++])) {
      if (seconds < format[0]) {
        if (typeof format[2] === 'string') {
          return format[list_choice];
        } else {
          return (
            Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token
          );
        }
      }
    }
    return time;
  }
}

export default new TimeUtils();
