import {StyleSheet} from 'react-native';

import cs from './cs';

const styles = StyleSheet.create({
  test0: {
    flex: 0,
  },
  test1: {
    width: '100%',
  },
  test2: {
    height: '100%',
  },
});

it('test if cs is working properly', () => {
  const output = cs(
    null,
    styles.test0,
    [styles.test1, true],
    [styles.test2, false],
  );

  const expected = [styles.test0, styles.test1];
  expect(output).toEqual(expected);
});
