import logger from './logger';

test('test if function returns', () => {
  expect(logger.log).toBeInstanceOf(Function);
});
