import {isArray} from './index';
export default (...classes) => {
  return classes
    .map((c) => {
      if (isArray(c) && c.length === 2) {
        return c[1] ? c[0] : false;
      }
      return c;
    })
    .filter((c) => !!c);
};
