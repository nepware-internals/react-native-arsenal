const _ = (store) => {
  return (key) => {
    const state = store.getState();
    const lang = state.lang;

    return lang.languages[lang.selectedLanguage][key] || key;
  };
};

export default _;
