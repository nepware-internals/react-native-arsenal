const noop = () => {};

class Logger {
  log(...params) {
    if (__DEV__) {
      return console.log(...params);
    }
    return noop;
  }
  error(...params) {
    if (__DEV__) {
      return console.error(...params);
    }
    return noop;
  }
}

export default new Logger();
